# KUBERNETES

### Kubernetes Pods
A Pod is a Kubernetes abstraction that represents a group of one or more application containers (such as Docker or rkt), 
and some shared resources for those containers
### Nodes
A Node is a worker machine in Kubernetes and may be either a virtual or a physical machine, depending on the cluster. Each Node is managed by the Master. A Node can have multiple pods, and the Kubernetes master automatically handles scheduling the pods across the Nodes in the cluster. 
### Kubelet
Kubelet, a process responsible for communication between the Kubernetes Master and the Node; it manages the Pods and the containers running on a machine
### kubectl
kubectl is the command-line interface (CLI) tool for working with a Kubernetes cluster.
### kubeadm
An administration tool that operates at the cluster level. You’ll use this to create your cluster and add additional nodes.